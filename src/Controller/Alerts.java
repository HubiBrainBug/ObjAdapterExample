package Controller;

import javafx.scene.control.Alert;

/**
 * Alerts zum Ausgeben von Informationen an den User
 */
public class Alerts {

    /**
     * Alert Erfolgreich: Eine Information wird ausgegeben
     * Land wurde erfolgreich in der Tabelle gepspeichert.
     */
    public static void alertErfolgreich () {
        Alert wurdegespeichert = new Alert (Alert.AlertType.INFORMATION);
        wurdegespeichert.setTitle ("Neues Land wurde erfolgreich gespeichert!");
        wurdegespeichert.setHeaderText ("Speicher Information:");
        wurdegespeichert.setContentText ("Das neue Land wurde erfolgreich gespeichert!");
        wurdegespeichert.showAndWait ();
    }

    /**
     * Alert Warning: Eine Warnung wird ausgegeben
     * Land konnte nich in Tabelle gespeichert werden
     */
    public static void alertWarnig () {
        Alert fail = new Alert (Alert.AlertType.WARNING);
        fail.setTitle ("Land konnte nicht gespeichert werden!");
        fail.setHeaderText ("Speicher Information:");
        fail.setContentText ("FEHLER!\n"
                + "Alle Felder müssen ausgefüllt werden.");
        fail.showAndWait ();
    }

    /**
     * Alert Delete: Eine Info, dass eine Zeile der Tabelle ausgefüllt sein muss
     * wenn gelöscht werden soll
     */
    public static void alertDelete () {
        Alert fail = new Alert (Alert.AlertType.INFORMATION);
        fail.setTitle ("Bitte eine Zeile auswählen!");
        fail.setHeaderText ("Information:");
        fail.setContentText ("FEHLER!\n"
                + "Eine Zeile muss geklickt sein!");
        fail.showAndWait ();
    }
}
