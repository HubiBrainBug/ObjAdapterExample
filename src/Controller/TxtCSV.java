package Controller;

import Model.Country;

import java.io.*;

/**
 * Klasse TxtCSV implementiert das Interface Export
 * bietet eine Methode um die Länder der Tabelle in ein CSV-File zu speichern
 */
public class TxtCSV implements Export {

    public static final String separator = ";";

    public static final String name = "CSV";

    /**
     * @return gibt den Namen der ExportFunktion zurück
     */
    @Override
    public String getFunktionName () {
        return "CSV";
    }

    /**
     * Die Methode versucht die Einträge der Tabelle in das CSV Format zu exportieren
     * @return true/false ob der Export funktioniert hat
     */
    @Override
    public boolean textSpeichern () {

        try {
            BufferedWriter bw = new BufferedWriter (new OutputStreamWriter (new FileOutputStream
                    ("mycountries.csv"), "UTF-16BE"));

            bw.write ("Meine Länder");
            bw.newLine ();
            bw.write ("Land;Hauptstadt;Währung;Kürzel");
            bw.newLine ();

            for(Country c : ControllerGUI.myWorld){
                StringBuffer sb = new StringBuffer ();
                sb.append (c.getLand () == null ? "" : c.getLand ());
                sb.append (separator);
                sb.append (c.getHauptstadt () == null ? "" : c.getHauptstadt ());
                sb.append (separator);
                sb.append (c.getWaehrung () == null ? "" : c.getWaehrung ());
                sb.append (separator);
                sb.append (c.getKuerzel () == null ? "" : c.getKuerzel ());
                bw.write (sb.toString ());
                bw.newLine ();
            }
            bw.flush ();
            bw.close ();
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace ();
            return false;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace ();
            return false;
        } catch (IOException e) {
            e.printStackTrace ();
            return false;
        }
    }
}
