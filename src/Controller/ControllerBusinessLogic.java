package Controller;

import javafx.collections.ObservableList;
import Model.Country;

/**
 * Klasse ControllerBusinessLogic bietet die Business Logic die vom GUI Controller aufgerufen wird.
 */
public class ControllerBusinessLogic {


    private final ObservableList<Export> exportFunctions;

    /**
     * Konstruktor:
     * Hier werden die einzelnen Optionen welche in der Choicebox möglich sind in eine Liste gespeichert
     */
    public ControllerBusinessLogic () { exportFunctions = ControllerGUI.exportOptions; }

    /**
     * @param land Name des Landes
     * @param hauptstadt Hauptstadt des Landes
     * @param waehrung Währung des Landes
     * @param kuerzel Kürzel des Landes
     * @return true/false wenn das Erstellen eines Objekts und hinzufügen zur Tabelle erfolgreich war oder nicht.
     */
    public boolean addNeuesLand (String land, String hauptstadt, String waehrung, String kuerzel) {
        if (land.equals ("") || hauptstadt.equals ("") || waehrung.equals ("") || kuerzel.equals ("")) {
            return false;
        } else {
            ControllerGUI.myWorld.add (new Country (land, hauptstadt, waehrung, kuerzel));
            return true;
        }
    }

    /**
     * Die Methode fileSpeichern versucht die Daten der Tabelle in ein File zu exportieren.
     * @param selectedItem das Feld das der User in der Choicebox gewählt hat. Dies ist das Format, in welches der
     *                     User seine Daten exportieren möchte.
     * @return true/false wenn die Speicherung erfolgreich war oder nicht.
     */
    public boolean fileSpeichern (String selectedItem) {
        for (Export e : exportFunctions) {
            if (e.getFunktionName ().equals (selectedItem)) {
                if(e.textSpeichern ()){
                System.out.println ("listerner hat funktioniert " + e.getFunktionName ());
                return true;
                }
            }
        }
        return false;
    }
}
